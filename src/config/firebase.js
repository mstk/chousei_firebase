import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyCAww0YmoxJQ4Y5tsWynMPW5oHCs3nBn-o",
  authDomain: "chousei-firebase-5277b.firebaseapp.com",
  databaseURL: "https://chousei-firebase-5277b.firebaseio.com",
  projectId: "chousei-firebase-5277b",
  storageBucket: "",
  messagingSenderId: "1007228027688",
  appId: "1:1007228027688:web:668444578996a1b4bac61d"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
